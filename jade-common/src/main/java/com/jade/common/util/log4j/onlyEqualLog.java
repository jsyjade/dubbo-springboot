package com.jade.common.util.log4j;

import org.apache.log4j.Priority;

public class onlyEqualLog  extends DailyRollingAndSizeFileAppender {

    @Override
    public boolean isAsSevereAsThreshold(Priority priority) {
        // 只判断是否相等，而不判断优先级
        return this.getThreshold().equals(priority);
    }

}