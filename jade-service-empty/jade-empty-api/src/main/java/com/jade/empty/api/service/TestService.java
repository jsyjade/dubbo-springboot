package com.jade.empty.api.service;

import com.jade.empty.api.model.TestDto;
import com.jade.empty.api.model.querymodel.TestQuery;

import java.util.List;

public interface TestService {
    List<TestDto> getTestList(TestQuery query);
    Integer insertTest(TestDto testDto);
}
