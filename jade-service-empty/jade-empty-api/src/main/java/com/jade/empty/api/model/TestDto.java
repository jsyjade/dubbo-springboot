package com.jade.empty.api.model;


import lombok.Data;

import java.io.Serializable;
@Data
public class TestDto implements Serializable {
    private Integer id;
    private String name;
    private String hobby;
}
