package com.jade.empty;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.jade.empty.*"})
@MapperScan(basePackages = {"com.jade.empty.dao"})
/**
 * 启动类
 */
public class JadeEmptyServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(JadeEmptyServiceApplication.class, args);
    }

}
