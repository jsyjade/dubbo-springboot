package com.jade.empty.service.impl;

import com.jade.empty.api.model.TestDto;
import com.jade.empty.api.model.querymodel.TestQuery;
import com.jade.empty.api.service.TestService;
import com.jade.empty.dao.TestDao;

import javax.annotation.Resource;
import java.util.List;

@org.apache.dubbo.config.annotation.Service(interfaceName="testService",version="${dubbo.service.version}")
public class TestServiceImpl implements TestService {
    @Resource
    TestDao testDao;
    @Override
    public List<TestDto> getTestList(TestQuery query) {
        return testDao.getTestList(query);
    }

    @Override
    public Integer insertTest(TestDto testDto) {
        return testDao.insertTest(testDto);
    }
}
