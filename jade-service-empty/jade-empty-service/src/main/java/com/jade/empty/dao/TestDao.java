package com.jade.empty.dao;

import com.jade.empty.api.model.TestDto;
import com.jade.empty.api.model.querymodel.TestQuery;

import java.util.List;

/**
 * @author jade
 * 接口
 */
public interface TestDao {
    /**
     * 获取test列表
     * @param query
     * @return
     */
    List<TestDto> getTestList(TestQuery query);

    /**
     * 添加信息
     * @param testDto
     * @return
     */
    Integer insertTest(TestDto testDto);
}
