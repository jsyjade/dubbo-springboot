package com.jade.empty2.dao;

import com.jade.empty2.api.model.OrderInfoDto;
import com.jade.empty2.api.model.querymodel.OrderInfoQuery;

import java.util.List;

/**
 * @author jade
 * 接口
 */
public interface OrderInfoDao {
    /**
     * 获取test列表
     * @param query
     * @return
     */
    List<OrderInfoDto> getTestList(OrderInfoQuery query);
    Integer insertOrderInfo(OrderInfoDto orderInfoDto);
}
