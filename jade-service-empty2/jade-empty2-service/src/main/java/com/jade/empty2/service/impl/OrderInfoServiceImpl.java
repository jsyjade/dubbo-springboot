package com.jade.empty2.service.impl;

import com.jade.empty2.api.service.OrderInfoService;
import com.jade.empty2.dao.OrderInfoDao;
import com.jade.empty2.api.model.OrderInfoDto;
import com.jade.empty2.api.model.querymodel.OrderInfoQuery;

import javax.annotation.Resource;
import java.util.List;

@org.apache.dubbo.config.annotation.Service(interfaceName="orderInfoService",version="${dubbo.service.version}")
public class OrderInfoServiceImpl implements OrderInfoService {
    @Resource
    OrderInfoDao orderInfoDao;
    @Override
    public List<OrderInfoDto> getTestList(OrderInfoQuery query) {
        return orderInfoDao.getTestList(query);
    }

    @Override
    public Integer insertOrderInfo(OrderInfoDto orderInfoDto) {
        return orderInfoDao.insertOrderInfo(orderInfoDto);
    }
}
