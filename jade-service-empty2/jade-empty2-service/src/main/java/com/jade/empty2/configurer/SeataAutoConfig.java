package com.jade.empty2.configurer;

import io.seata.spring.annotation.GlobalTransactionScanner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 全局扫面
 */
@Configuration
public class SeataAutoConfig {
    @Bean
    public GlobalTransactionScanner globalTransactionScanner() {
        return new GlobalTransactionScanner("jade-service-empty2", "my_test_tx_group");
    }
}
