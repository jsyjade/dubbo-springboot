package com.jade.empty2.api.service;

import com.jade.empty2.api.model.OrderInfoDto;
import com.jade.empty2.api.model.querymodel.OrderInfoQuery;

import java.util.List;

public interface OrderInfoService {
    List<OrderInfoDto> getTestList(OrderInfoQuery query);
    Integer insertOrderInfo(OrderInfoDto orderInfoDto);
}
