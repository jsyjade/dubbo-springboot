package com.jade.empty2.api.model;


import lombok.Data;

import java.io.Serializable;

@Data
public class OrderInfoDto implements Serializable {
    private String id;
    private String userId;
    private String commodityCode;
    private Integer count;
    private Integer money;
}
