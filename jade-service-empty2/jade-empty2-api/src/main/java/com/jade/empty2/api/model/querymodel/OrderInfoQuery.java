package com.jade.empty2.api.model.querymodel;

import lombok.Data;

import java.io.Serializable;

@Data
public class OrderInfoQuery implements Serializable {
    private String id;
    private String userId;
    private String commodityCode;
    private Integer count;
    private Integer money;
}
