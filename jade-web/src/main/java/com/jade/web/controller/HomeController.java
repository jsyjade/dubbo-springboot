package com.jade.web.controller;

import com.jade.empty.api.model.TestDto;
import com.jade.empty.api.model.querymodel.TestQuery;
import com.jade.empty.api.service.TestService;
import com.jade.web.globaltransaction.TestGTService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * @author Jade
 * 接口类
 */
@RestController
@RequestMapping("/home")
public class HomeController {
    @org.apache.dubbo.config.annotation.Reference(version = "${dubbo.service.version}")
    TestService testService;
    @Resource
    TestGTService testGTService;
    @RequestMapping("/getlist")
    //@ResponseBody
    public Object getList(@RequestParam(required = false)TestQuery query) throws Exception {
        List<TestDto> list=testService.getTestList(query);
        return list;
    }
    @RequestMapping("/addtest")
    public Object addTest() throws Exception{
        TestDto testDto=new TestDto();
        testDto.setName("test");
        testDto.setHobby("123");
        testService.insertTest(testDto);
        return "ok";
    }
    @RequestMapping("/add")
    public Object add() throws Exception{
        testGTService.insertTest();
        return "ok";
    }
}
