package com.jade.web.configurer;

import io.seata.spring.annotation.GlobalTransactionScanner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 全局扫面
 */
@Configuration
public class SeataAutoConfig {
    @Bean
    public GlobalTransactionScanner globalTransactionScanner() {
//        String applicationName = this.applicationContext.getEnvironment().getProperty("spring.application.name");
//        String txServiceGroup = this.seataProperties.getTxServiceGroup();
//        if (StringUtils.isEmpty(txServiceGroup)) {
//            txServiceGroup = applicationName + "-fescar-service-group";
//            this.seataProperties.setTxServiceGroup(txServiceGroup);
//        }
        return new GlobalTransactionScanner("jade-web", "my_test_tx_group");
    }
}
