package com.jade.web.configurer;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Configuration
public class WebConfig  {
    @Bean
    public HttpMessageConverters fastjsonHttpMessageConverter() {
        //定义一个转换消息的对象
        FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter() {
            @Override
            protected void writeInternal(Object obj, HttpOutputMessage outputMessage) throws IOException {
                try {
//                    HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
//                    String jsonpfunc = request.getParameter("callback");
//                    if (!StringUtils.isEmpty(jsonpfunc)) {
//                        OutputStream out = outputMessage.getBody();
//                        String jsonpText = jsonpfunc + "(" + JSON.toJSONString(obj) + ")";
//                        byte[] bytes = jsonpText.getBytes("UTF-8");
//                        out.write(bytes);
//                    } else {
//
//                    }
                    super.writeInternal(obj, outputMessage);
                } catch (IOException e) {
                    e.printStackTrace();
                    super.writeInternal(obj, outputMessage);
                }
            }
        };

        //添加fastjson的配置信息 比如 ：是否要格式化返回的json数据
        FastJsonConfig fastJsonConfig = new FastJsonConfig();

        fastJsonConfig.setSerializerFeatures(
                //格式化结果
                SerializerFeature.PrettyFormat,
                //字段为null，转""
                SerializerFeature.WriteNullStringAsEmpty,
                //list如果为空 输出[]而不是null
                SerializerFeature.WriteNullListAsEmpty,
                //输出为null的字段
                SerializerFeature.WriteMapNullValue,
                //格式化日期
                SerializerFeature.WriteDateUseDateFormat,
                //数值字段如果为null,输出为0,而非null
                SerializerFeature.WriteNullNumberAsZero,
                //Boolean字段如果为null,输出为false,而非null
                SerializerFeature.WriteNullBooleanAsFalse

        );
        //处理中文乱码问题
        List<MediaType> fastMediaTypes = new ArrayList<>();
        fastMediaTypes.add(MediaType.APPLICATION_JSON_UTF8);

        fastConverter.setSupportedMediaTypes(fastMediaTypes);
        //在转换器中添加配置信息
        fastConverter.setFastJsonConfig(fastJsonConfig);

        HttpMessageConverter<?> converter = fastConverter;

        return new HttpMessageConverters(converter);

    }
}

