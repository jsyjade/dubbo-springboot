package com.jade.web.globaltransaction;

import com.jade.empty.api.model.TestDto;
import com.jade.empty.api.service.TestService;
import com.jade.empty2.api.model.OrderInfoDto;
import com.jade.empty2.api.service.OrderInfoService;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.stereotype.Service;

/**
 * 业务-全局事务
 */
@Service
public class TestGTServiceImpl implements TestGTService {
    @org.apache.dubbo.config.annotation.Reference(version = "${dubbo.service.version}")
    OrderInfoService orderInfoService;
    @org.apache.dubbo.config.annotation.Reference(version = "${dubbo.service.version}")
    TestService testService;
    @Override
    @GlobalTransactional
    public int insertTest() throws Exception {
        TestDto testDto=new TestDto();
        testDto.setHobby("code");
        testDto.setName("jade");
        testService.insertTest(testDto);
        OrderInfoDto orderInfoDto=new OrderInfoDto();
        orderInfoDto.setCommodityCode("这是一个商品");
        orderInfoDto.setCount(100);
        orderInfoDto.setMoney(100);
        orderInfoDto.setUserId("110");
        orderInfoService.insertOrderInfo(orderInfoDto);
        return 0;
    }
}
