package com.jade.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JadeWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(JadeWebApplication.class, args);
    }

}
